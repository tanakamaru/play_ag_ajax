package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._
import models.Comment
import views._
import math._
import scala.util.parsing.json.JSON
import play.api.libs._
import play.api.libs.json._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.json4s.DynamicJValue._
import java.util.{ Date, Locale }
import java.text.DateFormat
import java.text.DateFormat._
import anorm.SqlParser._ 

case class Comment2(id: Long, comment: String)

object Application extends Controller {
  
  val taskForm = Form(
    mapping(
      "id" -> ignored(0L),
      "label" -> nonEmptyText)(Comment.apply)(Comment.unapply))
  val commentForm = Form(
    "comment" -> nonEmptyText)

  def index = Action {
    Ok(views.html.index(Comment.all))
  }
  
  def getTest = Action { implicit request =>
    //val test:List[Comment] = Comment.all2
    //var aaa =("id",test)
    //val count  = Comment.countAll
    val commetList = Comment.all2
    
    var mapJson:Map[String,String] = Map()
    var list: List[String] = List()
    
    
    for(i <- 0 to commetList.length-1){
    	val testtt = commetList(i).id
    	
     	val target = commetList(i).comment
    	val startWithA = "(\".+?\")".r
    	target match {
    		case startWithA(matched) => mapJson += commetList(i).id.toString -> commetList(i).comment.init.tail
    		case _ => mapJson += commetList(i).id.toString -> commetList(i).comment
    	}
    	
    	//list = commetList(i).comment.init.tail :: list
    	//mapJson += commetList(i).id.toString -> commetList(i).comment.init.tail
    }
    
    list = list.reverse
    val fin = Json.toJson(list)
    //val jsonList = List("apple", "orange", "banana")
    val aaa = Json.toJson(mapJson)
    Ok(aaa)
  }

  def postComment(comment: String) = Action { implicit request =>
    Comment.insertDB(comment)
    Ok
  }

  def postDelete(id: Long) = Action { implicit request =>
    Comment.deleteDB(id)
    Ok
  }
  
  def postKillRow(id: Long) = Action { implicit request =>
    Comment.deleteRowDB(id)
    Ok
  }
  
  def postEditUpdate(id: Long,commentval: String) = Action { implicit request =>
    Comment.editUpdate(id,commentval)
    Ok
  }

  def newTask = Action { implicit request =>
    val refTest = request.body.asFormUrlEncoded
    val jsonObject = Json.toJson(request.body.asFormUrlEncoded)
    
    val obj = Json.toJson(jsonObject.\("label")(0))
    val objn = Json.toJson(jsonObject.\("name")(0))
    
    
    /*val jsVal = Json.parse("""{"name":"taro"}""")
    val jsStr = Json.stringify(JsObject("name" -> JsString("taro") :: Nil))
    val jsValStr = Json.toJson("taro") //JsValue = "taro"
    val jsValInt =  Json.toJson(1) //JsValue = 1
    val m = Map("name"-> "taro","email" -> "taro@taro.com")
    val jsValMap = Json.toJson(m)
    
    val now = new Date
    val df = getDateInstance(LONG, Locale.FRANCE)*/
    Comment.postCreate(obj.toString)
    val lastIdAll = Comment.lastInsertSelect
    val lastId = lastIdAll(0).id.toString
    Ok(lastId)
  }
  //val t = taskForm.bindFromRequest.value map { task =>
      //println(request)
      //println("━━━━━━━━━━━━━━━━(ﾟ∀ﾟ)━━━━━━━━━━━━━━━━")
      //Comment.postCreate("aaaa2014/03/18")
      //t
    //}
    //Comment.postCreate()
    //Ok
  
  
  def nextPage = Action { implicit request =>
    Ok(views.html.next())
  }

  def javascriptRoutes = Action { implicit request =>
    import routes.javascript._
    Ok(
      Routes.javascriptRouter("jsRoutes", Some("jQuery.ajax"))(
        routes.javascript.Application.index,
        routes.javascript.Application.postComment,
        routes.javascript.Application.postDelete,
        routes.javascript.Application.getTest,
        routes.javascript.Application.postKillRow,
        routes.javascript.Application.postEditUpdate,
        routes.javascript.Application.newTask)).as("text/javascript")
  }
}
