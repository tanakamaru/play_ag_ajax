package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current

case class Comment(id: Long, comment: String)
case class LastId(id:Long)

object Comment {

  val comments = {
    get[Long]("id") ~
    get[String]("comment") map {
      case id ~ comment => Comment(id, comment)
    }
  }
  val lastId = {
        get[Long]("id") map {
            case id => LastId(id)
        }
    }

  
  def all: List[Comment] = DB.withConnection { implicit c =>
    SQL("select * from comment").as(comments *)
  }
  
  def all2: List[Comment]= DB.withConnection { implicit c =>
    val dates = SQL("select * from comment").as(Comment.comments *)
    return dates
  }

  def countAll: Long = { 
    DB.withConnection { implicit connection =>
      SQL("SELECT COUNT(*) FROM comment").as(scalar[Long].single)
    }
  }
  
  def lastInsertSelect: List[LastId] = DB.withConnection { implicit c =>
    var ttt = SQL("SELECT id FROM comment ORDER BY id desc LIMIT 1").as(lastId *)
    return ttt
  }
  
  def editUpdate(id: Long,newVal: String){DB.withConnection { implicit c =>
      val dates = SQL("UPDATE comment SET comment = {newVal} WHERE id = {id}").on(
        'newVal ->newVal ,'id -> id
      ).executeUpdate()
  	}
  }
    
  
  def deleteRowDB(id: Long){
    DB.withConnection { implicit c =>
      SQL("""
          DELETE FROM comment WHERE id =
          {id}
          """
      ).on(
        'id -> id
      ).executeUpdate()
    }
  }
  
  
  def insertDB(comment: String) {
    DB.withConnection { implicit c =>
      SQL("""
          insert into comment (comment)
          values ({comment})
          """
      ).on(
        'comment -> comment
      ).executeUpdate()
    }
  }
  
  def deleteDB(id: Long){
    DB.withConnection { implicit c =>
      SQL("""
          DELETE FROM comment WHERE id =
          {id}
          """
      ).on(
        'id -> id
      ).executeUpdate()
    }
  }

  def postCreate(comment: String){
    DB.withConnection { implicit c =>
      SQL("""
          insert into comment (comment)
          values ({comment})
          """
      ).on(
        'comment -> comment
      ).executeUpdate()
    }
  }
  
}