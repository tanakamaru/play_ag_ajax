name := "ag3"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.29",
  "org.json4s" %% "json4s-native" % "3.2.7",
  "org.json4s" %% "json4s-jackson" % "3.2.7",
  "net.liftweb" % "lift-json_2.9.1" % "2.4"
)     

play.Project.playScalaSettings
