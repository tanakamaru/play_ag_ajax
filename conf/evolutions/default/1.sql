# Tasks schema
 
# --- !Ups

#CREATE SEQUENCE task_id_seq;
#CREATE TABLE task (
#    id integer NOT NULL DEFAULT nextval('task_id_seq'),
#    label varchar(255)
#);
 
CREATE TABLE task (
    id integer NOT NULL AUTO_INCREMENT,
    label varchar(255),
PRIMARY KEY (id)
);
# --- !Downs
 
DROP TABLE task;
DROP SEQUENCE task_id_seq;



