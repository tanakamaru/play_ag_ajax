      // モデル
      function Todo(text, done) {
        this.id = Todo.getNextId();
        this.codeId = 0;
        this.text = text;
        this.done = done || false;
        this.editing = false;
        this.row = false;
      }
      
      // 連番の ID を作成
      Todo.getNextId = (function() {
        var _nextId = 0;
        var _getNextId = function() {
          _nextId++;
          return _nextId;
        };
        return _getNextId;
      })();

      // コントローラー
      function TodoCtrl($scope) {
    	  $scope.categories = [
    	                       {name:'スマートフォン', value:'smartPhone'},
    	                       {name:'タブレット', value:'tablet'},
    	                       {name:'ノートPC', value:'notepc'},
    	                       {name:'デスクトップPC', value:'desktop'}
    	                   ];
    	                   $scope.category =  $scope.categories[0].value;
    	                   
    	  $scope.todos = [
    	                  //new Todo("foo", true),
    	                  //new Todo("bar")
    	                ];
    	  jsRoutes.controllers.Application.getTest().ajax({
				'success' : function(data){
					//alert(data);
					$.getJSON('', function(data) {
				        $scope.$apply(function(){
				            $scope.modelData = data;
				        });
				    });
					var i = 1
					for (key in data) {
						_datum = data[key];
				        $scope.$apply(function() {
				        	$scope.todos.push({id: i, codeId: key, text:_datum, row: i});
				        	//new Todo(_datum));
				        	i++;
					    });
					}
					//for (var i = 0; i < data.length; i++) {
				        
				    //}

					
				},
	      	      error: function() {
	        	        alert("error:index");
	        	      }
			});
    	 
        $scope.todoName = {
        	    name: '三代目名人川口',
        	    Address: 'Tokyo'
        };
        

        // 未完了の ToDo を数える
        $scope.remaining = function() {
          var count = 0;
          angular.forEach($scope.todos, function(todo) {
            count += todo.done ? 0 : 1;
          });
          return count;
        };

        // 完了した ToDo を削除
        $scope.archive = function() {
          var oldTodos = $scope.todos;
          $scope.todos = [];
          angular.forEach(oldTodos, function(todo) {
            if (!todo.done) {
              $scope.todos.push(todo);
            }
          });
        };

        // ToDo 追加
        $scope.addTodo = function() {
          //$scope.todos.push(new Todo($scope.todoText));
          //$scope.todoText = "";
        	console.log($scope.todoName);
        	jsRoutes.controllers.Application.newTask().ajax({
      	      data: { label: $scope.todoText,
      	    	      name: $scope.todoName.name,
      	    	      category:$scope.category },
      	      success: function(data) {
      	         $scope.$apply(function(){
      	        	console.log(data);
      	        	var todoc = new Todo($scope.todoText);
      	        	todoc.codeId = data;
      	            $scope.todos.push(todoc);
      	            $scope.todoText = '';
      	         });
      	      },
      	      error: function() {
      	        alert("error:addTask");
      	      }
      	    });
          
        };

        // ToDo の編集開始
        $scope.editTodo = function(id) {
          var todo = findTodoById(id);
          todo.editing = true;
        };

        // ToDo の編集終了
        $scope.updateTodo = function(id,codeId) {
          var todo = findTodoById(id);
          //todo.editing = false;
          jsRoutes.controllers.Application.postEditUpdate(codeId,todo.text).ajax({
  			'success' : function(data) {
  				$scope.$apply(function() {
  					todo.editing = false;
  				});
  			},
  			error : function() {
  				alert("error:UPDATE");
  			}
  		});
        };

        // ToDo を削除
        $scope.deleteTodo = function(id,codeid) {
        	var idData= parseFloat(codeid);
    		var index = findTodoIndexById(id);
    		console.log(codeid + id);
    		jsRoutes.controllers.Application.postKillRow(idData).ajax({
    			'success' : function(data) {
    				$scope.$apply(function() {
    					console.log($scope.todos);
    					$scope.todos.splice(index, 1);
    				});
    			},
    			error : function() {
    				alert("error:delete");
    			}
    		});
    	};

        // Todo を ID で検索してインデックスを取得
        function findTodoIndexById(id) {
          var numId = Number(id);
          for (var i = 0, max = $scope.todos.length; i < max; i++) {
            var todo = $scope.todos[i];
            if (todo.id === numId) {
              return i;
            }
          }
          return null;
        }

        // Todo を ID で検索
        function findTodoById(id) {
          var index = findTodoIndexById(id);
          return $scope.todos[index];
        }
      }
      
      var ngbootcamp = angular.module('ngbootcamp', []);
      
      ngbootcamp.controller('todoCtl', function($scope){
          var force = new remotetk.Client();
          var soql = "select Id, Name from guest__c";
          force.query(
              soql,
              function(result){
                  $scope.guests = result.records;
                  $scope.$apply();
              },
                  function(result){
              }
          );
      });
      